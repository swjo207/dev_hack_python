import socket
import threading
import SocketServer

class ThreadedTCPRequestHandler(SocketServer.BaseRequestHandler):
	def handler(self):
		data = self.request_recv(1024)
		cur_thread = threading.current_thread()
		response = "{}:{}".format(cur_thread.name,data)
		self.request_send(response)

class ThreadedTCPServer(SocketServer.ThreadingMixin,SocketServer.TCPServer):
	pass

def client(ip,port,message):
	sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
	sock.connect((ip,port))
	try:
		sock.send(message)
		response = sock.recv(1024)
		print "Received: {}".format(response)
	finally:
		sock.close()

if __name__ == "__main__":
	HOST,PORT = "localhost", 0

	server = ThreadedTCPServer((HOST,PORT),ThreadedTCPRequestHandler)
	ip, port = server.server_address

	server_thread = threading.Thread(target=server.serve_forever)

	server_thread.daemon = True
	server_thread.start()
	print "Server loop running in thread:", server_thread.name

	client(ip, port, "Hellow World 1")

	server.shutdown()
