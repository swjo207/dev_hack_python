import mechanize, cookielib, random

class anonBrowser(mechanize.Browser):
	def __init__(self, proxies = [], user_agents = []):
		mechanize.Browser.__init__(self)
		self.set_handle_robots(False)
		self.proxies = proxies
		self.user_agent = user_agents + ['Mozilla/4.0 ', \
			'FireFox/6.01', 'ExactSearch', 'Nokia7110/1.0']
		self.cookie_jar = cookielib.LWPCookieJar()
		self.set_cookiejar(self.cookie_jar)
		self.anonymize()

	def	clear_cookies(self):
		self.cookie_jar = cookielib.LWPCookieJar()
		self.set_cookiejar(self.cookie_jar)

	def	change_user_agent(self):
		index = random.randrange(0, len(self.user_agent))
		self.addheaders = [('User-agent', (self.user_agent[index]))] 
	
	def	change_proxy(self):
		if self.proxies:
			index = random.randrange(0, len(self.proxies))
			self.set_proxies({'http': self.proxies[index]})
	def	anonymize(self, sleep = False):
		self.clear_cookies()
		self.change_user_agent()
		self.change_proxy()
		if sleep:
			time.sleep(60)
		
def viewPage(url):
	browser = mechanize.Browser()
	page = browser.open(url)
	source_code = page.read()
	print source_code

def testProxy(url,proxy):
	browser = mechanize.Browser()
	browser.set_proxies(proxy)
	page = browser.open(url)
	source_code = page.read()
	print source_code

def testUserAgent(url,userAgent):
	browser = mechanize.Browser()
	browser.addheaders = userAgent
	page = browser.open(url)
	source_code = page.read()
	print source_code

def printCookies(url):
	browser = mechanize.Browser()
	cookie_jar = cookielib.LWPCookieJar()
	browser.set_cookiejar(cookie_jar)
	page = browser.open(url)
	for cookie in cookie_jar:
		print cookie
	
"""viewPage('http://www.syngress.com/')"""

"""
url = 'http://google.com/'
hideMeProxy = {'http': '120.198.232.199:80'}	
testProxy(url,hideMeProxy)
"""
"""
url = 'http://whatismyuseragent.dotdoh.com/'
userAgent = [('User-agent', 'Mozilla/5.0 (X11; U; '+ \
	'Linux 2.4.2-2 i586; en-US; m18) Gecko/20011020 Netscape6/6.01')]
testUserAgent(url,userAgent)
"""
url = 'http://www.syngress.com/'
printCookies(url)

